# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.


class TowersOfHanoi
  attr_reader :towers

  def initialize
    @towers = [[3, 2, 1], [], []]
  end

  def play
    puts 'Welcome to Towers of Hanoi'
    until won?
      render
      next_move = input_move
      puts 'Invalid Move' unless valid_move?(next_move[0], next_move[1])
      next unless valid_move?(next_move[0], next_move[1])
      move(next_move[0], next_move[1])
    end
    puts 'Puzzle Solved!'
    render
  end

  def render
    display_hash = { nil => '   |   ', 1 => '  *|*  ',
                     2 => ' **|** ', 3 => '***|***' }
    display = [[], [], []]
    display.each_index do |idx|
      @towers.each { |tower| display[idx] << display_hash[tower[idx]] }
    end
    display << []
    display.reverse!
    display << ['   0   ', '   1   ', '   2   ']
    display.each { |line| puts '  ' + line.join }
  end

  def won?
    @towers[0].empty? && (@towers[1].empty? || @towers[2].empty?)
  end

  def valid_move?(from, to)
    return false unless (0..2).cover?(from) && (0..2).cover?(to)
    return false if @towers[from].empty?
    return true if @towers[to].empty?
    return false if @towers[from].last > @towers[to].last
    true
  end

  def move(from_tower, to_tower)
    @towers[to_tower].push(@towers[from_tower].pop)
  end

  def input_move
    puts 'Select a tower to move a disc from:'
    from = gets.chomp
    puts 'Setect a tower to move the disc to to:'
    to = gets.chomp
    [from.to_i, to.to_i]
  end
end

if $PROGRAM_NAME == __FILE__
  game = TowersOfHanoi.new
  game.play
end
